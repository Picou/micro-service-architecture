package com.example.frontwebservice;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class FrontWebService {

    @Autowired
    DiscoveryClient discoveryClient;

    @GetMapping("/")
    @HystrixCommand(fallbackMethod = "defaultMessage")
    public String hello() {
        List<ServiceInstance> instances = discoveryClient.getInstances("name-of-the-microservice1");
        ServiceInstance test = instances.get(0);
        String hostname = test.getHost();
        int port = test.getPort();

        RestTemplate restTemplate = new RestTemplate();
        String microserviceAddress = "http://" + hostname + ":" + port;
        ResponseEntity<String> response = restTemplate.getForEntity(microserviceAddress, String.class);

        return response.getBody();
    }

    public String defaultMessage() {
        return "'sup";
    }

}
